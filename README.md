# SI propject 3

https://gitlab.com/cphbus-si-project3/frontend

https://gitlab.com/cphbus-si-project3/backend

This project is split up in a frontend and a backend container, both exposing a web api.

The frontend is exposed to the public internet using a loadbalancer that is attached to a public ip.

The backend is ONLY exposed to the cluster.

Both deployments (a deployment is a collection of pods) has a kubernetes service attached to them.

The backend will be accessed from the other containers, using the service name.

You can view the frontend at this ip: http://192.158.28.175/

## Reproduce it

Start by login to google cloud - on the local machine with the cloud sdk - or the online version

The presentation might help you

### Create cluster

gcloud config set compute/zone europe-west1-b

gcloud container clusters create "production" --machine-type "g1-small" --scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "1" --enable-cloud-logging --enable-cloud-monitoring                                     

### IP

We need a static ip to be exposed with on the load balancer "VPC network > External IP addresses"  
It has to be to the premium network if you use the google cloud projects default settings

### Database

We created a cloud sql database with mysql, important to note is to set the authorized network 0.0.0.0/0 for access from anywhere

#### Scheme

schema: feedback  
table: RatingTab  
    location        varchar(255)  
    gender          varchar(255)  
    age             int  
    rating          int  
    description     varchar(255)  

### Update kube.yaml

Insert the new ip and the database information into 

deploy it wuth `kubectl apply -f kube.yaml`

## Set up ci

Create a service account for gitlab in "IAM & admin > service account"

give it a name

give it access to kubernetes engine ("Kuberneted Engine Admin" can be used, but may be to open)

generate a json file

put the text into a gitlab ci env variable with name "SERVICE_ACCOUNT" (we removed all new lines from the file, which may or may not be necessary)

all new commits will now be pushed automaticly

